<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-16
 * Time: 00:16
 */


    /**
     * Class for collecting all configuration settings given in file config.php,
     * and for sharing them on demand.
     */
class Config {
    private static $instance;
    private $host;
    private $port;
    private $dbname;
    private $user;
    private $password;
    private $debug;
    private $messageSize;
    private $usernameSize;
    private $allowedChars;

    /**
     * Config constructor. Fetches all data from config.php.
     */
    private function __construct() {
        require_once __DIR__ . '/../config.php';
        $this->host = $host;
        $this->port = $port;
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
        $this->debug = $debug;
        $this->messageSize = $messageSize;
        $this->usernameSize = $usernameSize;
        $this->allowedChars = $allowedChars;
    }


    /**
     * The singleton "constructor". Returns the only instance of the class.
     * @return self Instance of config class.
     */
    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Config();
        }
        return self::$instance;
    }


    /**
     * Get function for the debug-setting.
     * @return mixed Debug setting.
     */
    public function checkDebug() {
        return $this->debug;
    }


    /**
     * Function producing the login string for communication with databse.
     * @return mixed|string The database login string for Postgre database
     */
    public function getDbDsn(){
        $dsn = 'host=' . $this->host . ' port=' . $this->port . ' dbname=' . $this->dbname .
            ' user=' . $this->user . ' password=' . $this->password;
        return $dsn;
    }

    /**
     * Function return the allowed size for message
     */
    public function getMaxMessageSize() { return $this->messageSize; }

    public function getUsernameLimit() { return $this->usernameSize; }

    public function getLegalCharacters() { return $this->allowedChars; }

}