<?php
/**
 * Created by PhpStorm.
 * User: mattias
 * Date: 2018-05-17
 * Time: 17:21
 */

include_once 'util.php';

// Default response text
$responseText['success'] = false;
$responseText['messageId'] = '';
$responseText['displayMessage'] = '';

session_start();

// delete a message
if (isset($_GET['task']) && $_GET['task'] === 'delete' && isset($_GET['id'])) {
    if (isset($_SESSION['loggedIn']) && isset($_SESSION['user'])) {
        // Check who the creator of the message with the given id is
        $db = Database::getInstance();
        if ($db->connect())
            $creatorName = $db->getUsernameByMessageId($_GET['id']);
        $db->disconnect();

        // if the creator of the message is the logged in user, delete the message
        if ($creatorName === $_SESSION['user']->getUsername()) {
            if ($db->connect() && $db->deleteMessage($_GET['id'])) {
                $responseText['success'] = true;
                $responseText['messageId'] = $_GET['id'];
                $responseText['displayMessage'] = 'The message was successfully deleted';
            }
            $db->disconnect();
        }
    }
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);