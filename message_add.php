<?php
/**
 * Created by PhpStorm.
 * User: dreadflopp
 * Date: 2018-05-19
 * Time: 17:16
 */

include_once 'util.php';

// default response message
$responseText['success'] = false;
$responseText['displayMessage'] = 'You must be logged in to post';
$responseText['addedMessage'] = null;

session_start();

// Check that user is logged in
if (isset($_SESSION['loggedIn']) && isset($_SESSION['user'])) {
    // Get max size of messages
    $messageMaxSize = Config::getInstance()->getMaxMessageSize();

    // Check that a text is posted and that it isn't empty
    if (isset($_POST['text']) && $_POST['text'] != '') {
        if (strlen($_POST['text']) <= $messageMaxSize) {
            $sanitizedText = sanitize($_POST['text']);
            // add message to database
            $db = Database::getInstance();
            $db->connect();
            $username = $_SESSION['user']->getUsername();
            $result = $db->addMessage($username, $_POST['text']);

            // set response
            if ($result['success']) {
                $responseText['success'] = true;
                $responseText['displayMessage'] = 'Message added';
                $responseText['addedMessage'] = $db->getMessageById($result['messageId']);
            } else {
                $responseText['success'] = false;
                $responseText['displayMessage'] = $result['message'];
            }
            $db->disconnect();
        }
        else {
            $responseText['displayMessage'] = 'Message can be no longer than ' . $messageMaxSize . ' characters.';
        }
    }
}
else
    $responseText['displayMessage'] = 'You must be logged in to post';

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);