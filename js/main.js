/* different variables to store XMLHttpRequest objects.
    Using different variables for different type of calls keeps requests
    separated from each other, allowing simultaneous requests while
    making sure the responses reaches the correct event handlers
 */
var xhr_login;  // used for login/logout
var xhr_delMsg; // used for deleting messages
var xhr_addMsg; // used for adding messages
var xhr_getMsg; // used for getting messages
var xhr_register; // used for user registration
var xhr_reset; // used to reset forgotten passwords
var xhr_voteMsg;// used for voting on messages

//Utilities
function byId(id) { return document.getElementById(id); }
function elem(id) { return document.createElement(id); }


/**
 * Main function run on load of each page. sets up the request object and
 * sends GET request to PHP for information regarding logged in status.
 */
function main() {
    // Register event listeners
    byId("loginButton").addEventListener('click', doLogin, false);
    byId("logoutButton").addEventListener('click', doLogout, false);
    byId("postButton").addEventListener('click', doAddMessage, false);
    byId("registerButton").addEventListener('click', openRegistration, false);
    byId("resetButton").addEventListener('click', openReset, false);
    byId("closeRegistration").addEventListener('click', closeRegistration, false);
    byId("closeReset").addEventListener('click', closeReset, false);
    byId("confirmRegistration").addEventListener('click', doRegistration, false);
    byId("confirmReset").addEventListener('click', doPasswordReset, false);
    byId("sortBySelect").addEventListener('change', isLoggedIn, false);
    byId("showUserTextBox").addEventListener('input', isLoggedIn, false);
    byId("searchWordTextBox").addEventListener('input', isLoggedIn, false);

    // Initialize xhr objects
    try {
        if (window.XMLHttpRequest) {
            xhr_login = new XMLHttpRequest();
            xhr_delMsg = new XMLHttpRequest();
            xhr_addMsg = new XMLHttpRequest();
            xhr_getMsg = new XMLHttpRequest();
            xhr_register = new XMLHttpRequest();
            xhr_reset = new XMLHttpRequest();
            xhr_voteMsg= new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            xhr_login = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_delMsg = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_addMsg = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_getMsg = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_register = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_reset = new ActiveXObject("Microsoft.XMLHTTP");
            xhr_voteMsg = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            throw new Error('Cannot create XMLHttpRequest object');
        }
    } catch (e) {
        alert('"XMLHttpRequest failed!' + e.message);
    }

    // login
    xhr_login.addEventListener('readystatechange', processLogin, false);
    xhr_login.open('GET', 'login.php', true);
    xhr_login.send(null);
}
window.addEventListener("load", main, false);




//       Functions for control of logged in/logged out status                //
//---------------------------------------------------------------------------//

/**
 * Handles the logged in status response from PHP and directs to functions
 * appropriate for setting up the page depending on if the user is logged
 * in or not.
 */
function processLogin() {
    if (xhr_login.readyState === XMLHttpRequest.DONE && xhr_login.status === 200) {
        xhr_login.removeEventListener('readystatechange', processLogin, false);
        var myResponse = JSON.parse(this.responseText);
        if (myResponse.loggedIn) {
            byId('loginSuccess').innerText = myResponse.displayMessage;
            byId('login').style.display = "none";
            byId('logout').style.display = "initial";
            byId("uname").value = "";
            byId("psw").value = "";
            byId('writeMessageTextArea').style.display = "initial";
        }
        else {
            byId('loginFailure').innerText = myResponse.displayMessage;
        }
        messagesModule.set(myResponse.messages);
        messagesModule.show(myResponse.username);
    }
}

/**
 * Function called by the login button event listener. Sends a POST to PHP holding
 * the data entered in the login fields. Directs answer to function processLogin.
 */
function doLogin() {

    var  uname, psw;
    if (((uname = byId('uname').value) !== "") && ((psw = byId('psw').value) !== "")) {
        xhr_login.addEventListener('readystatechange', processLogin, false);
        xhr_login.open('POST', 'login.php', true);
        xhr_login.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr_login.send("login=yes" + "&uname=" + uname + "&psw=" + psw);
    }
}
/**
 * Handles the logout response from PHP by returning the page display to logged
 * out mode and redirection to index.php.
 */
function processLogout() {
    if (xhr_login.readyState === XMLHttpRequest.DONE && xhr_login.status === 200) {
        xhr_login.removeEventListener('readystatechange', processLogout, false);
        location.reload();
        byId('login').style.display = "initial";
        byId('logout').style.display = "none";
    }
}

/**
 * Function called by the logout button event listener. Calls PHP with empty
 * GET request and directs answer to processLogout. *
 */
function doLogout() {
    xhr_login.addEventListener('readystatechange', processLogout, false);
    xhr_login.open('GET', 'logout.php', true);
    xhr_login.send(null);
}

/**
 * Logs in if there is an ongoing session, updates the page
 */
function isLoggedIn() {
    // Try to login (checks if there is an ongoing session)
    xhr_login.addEventListener('readystatechange', processLogin, false);
    xhr_login.open('GET', 'login.php', true);
    xhr_login.send(null);
}



//               Functions handling message voting                           //
//---------------------------------------------------------------------------//

/**
 * Function called when user clicks an active vote button.
 * @param loggedInUser The currently logged in user.
 * @param voteButton The clicked vote button element.
 */
function doVoteMessage(loggedInUser, voteButton) {
    if (voteButton.parentNode.parentNode.getAttribute('data-author') !== loggedInUser) {
        var vote = 0;
        if (voteButton.classList.contains('upVoteButton')) {
            vote = 1;
        } else if (voteButton.classList.contains('downVoteButton')) {
            vote = -1;
        }
        xhr_voteMsg.addEventListener('readystatechange', processVoteMessage, false);
        xhr_voteMsg.open('POST', 'message_vote.php', true);
        xhr_voteMsg.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr_voteMsg.send("msgId=" + voteButton.parentNode.parentNode.id + "&vote=" + vote);
    }
}

/**
 * Handling of the clicked vote button response from PHP.
 */
function processVoteMessage() {
    if (xhr_voteMsg.readyState === XMLHttpRequest.DONE && xhr_voteMsg.status === 200) {
        xhr_voteMsg.removeEventListener('readystatechange', processVoteMessage, false);
        var response = JSON.parse(this.responseText);
        if (response['result'] === 'failure')
            alert('Failure placing the vote');
        else {
            var message = document.getElementById(response['messageId']);
            message.getElementsByClassName('voteCount').innerText = response['newVote'];
            isLoggedIn();
        }
    }
}


//               Functions handling deletion of messages                     //
//---------------------------------------------------------------------------//

/**
 * Function called by the delete buttons
 */
function doDeleteMessage() {
    xhr_delMsg.addEventListener('readystatechange', processDeleteMessage, false);
    xhr_delMsg.open('GET', 'message_delete.php?task=delete&id=' + this.parentNode.parentNode.id, true);
    xhr_delMsg.send(null);
}

/**
 * Handle the response from PHP that deletes messages
 */
function processDeleteMessage() {
    if (xhr_delMsg.readyState === XMLHttpRequest.DONE && xhr_delMsg.status === 200) {
        xhr_delMsg.removeEventListener('readystatechange', processDeleteMessage, false);
        var myResponse = JSON.parse(this.responseText);

        // update the page by deleting message if it was successfully deleted from the db
        if (myResponse['success'] === true) {
            messagesModule.remove(myResponse['messageId']);
        }
    }
}


//               Functions handling addition of messages                     //
//---------------------------------------------------------------------------//

/**
 * Function called by the post message button
 */
function doAddMessage() {
    // Get text
    var text = byId('messageBody').value;

    // If message is empty
    if (text.length <= 0) {
        // Set status message
        byId('postMessageStatus').innerHTML = "The message can not be empty";
    }

    // if message is not empty
    else {
        // Create formdata
        var formData = new FormData();
        formData.append('text', text);

        // create xhr request
        xhr_addMsg.addEventListener('readystatechange', processAddMessage, false);
        xhr_addMsg.open('POST', 'message_add.php', true);
        xhr_addMsg.send(formData);

        // set status message
        byId('postMessageStatus').innerHTML = 'Processing...';
    }
}

/**
 * Function handling the add message response from PHP.
 */
function processAddMessage() {
    if (xhr_addMsg.readyState === XMLHttpRequest.DONE && xhr_addMsg.status === 200) {
        xhr_addMsg.removeEventListener('readystatechange', processAddMessage, false);
        var myResponse = JSON.parse(this.responseText);

        // set status message
        byId('postMessageStatus').innerHTML = myResponse['displayMessage'];

        // add message
        messagesModule.add(myResponse['addedMessage']);
        messagesModule.show();

        //Clear the write message text box.
        byId('messageBody').value = '';

        // do a login-check to add delete buttons
        isLoggedIn();
    }
}

function doRegistration() {
    var post = true,
        username = byId("regUsername").value,
        password = byId("regPassword").value,
        repeatPassword = byId("regPswdConfirm").value,
        email = byId("regEmail").value;

    byId("registrationFailure").innerHTML = "";

    if (username <= 0) {
        byId("registrationFailure").innerHTML += "You must choose a username<br>";
        post = false;
    }
    if (password <= 0) {
        byId("registrationFailure").innerHTML += "You must choose a password<br>";
        post = false;
    }
    if (password !== repeatPassword) {
        byId("registrationFailure").innerHTML += "Passwords do not match<br>";
        post = false;
    }
    if (email <= 0) {
        byId("registrationFailure").innerHTML += "You must supply a valid email<br>";
        post = false;
    }

    if(post) {
        // Create formdata
        var formData = new FormData();

        formData.append('username', username);
        formData.append('password', password);
        formData.append('email', email);

        // create xhr request
        xhr_register.addEventListener('readystatechange', processRegistration, false);
        xhr_register.open('POST', 'register.php', true);
        xhr_register.send(formData);
    }
    else {
        // Resets password values on failed registration
        byId("regPassword").value = "";
        byId("regPswdConfirm").value = "";
    }
}

function processRegistration() {
    if (xhr_register.readyState === XMLHttpRequest.DONE && xhr_register.status === 200) {
        xhr_register.removeEventListener('readystatechange', processRegistration, false);

        var response = JSON.parse(this.responseText);

        if(!response.result) {
            for (var i = 0; i < response.errors.length; i++) {
                byId("registrationFailure").innerHTML += response.errors[i] + "<br>";
            }

            byId("regPassword").value = "";
            byId("regPswdConfirm").value = "";
        }
        if(response.result) {
            closeRegistration();
            alert("Your account has been successfully created. Thank you for registering.")
        }
    }
}

function doPasswordReset() {
    var post = true,
        email = byId("resetEmail").value;

    if (email <= 0) {
        byId("registrationFailure").innerHTML += "You must supply a valid email<br>";
        post = false;
    }

    if (post) {
        // Create formdata
        var formData = new FormData();

        formData.append('email', email);

        // create xhr request
        xhr_reset.addEventListener('readystatechange', processPasswordReset, false);
        xhr_reset.open('POST', 'request_pw_reset.php', true);
        xhr_reset.send(formData);
    }
    else {
        byId("resetFailure").innerHTML += "You must supply a valid email<br>";
        byId("resetForm").reset();
    }
}



function processPasswordReset() {
    if (xhr_reset.readyState === XMLHttpRequest.DONE && xhr_reset.status === 200) {
        xhr_reset.removeEventListener('readystatechange', processPasswordReset, false);

        var response = JSON.parse(this.responseText);

        if(!response.result) {
            for (var i = 0; i < response.errors.length; i++) {
                byId("resetFailure").innerHTML += response.errors[i] + "<br>";
            }
            byId("resetForm").reset();
        }

        if(response.result) {
            console.log(response.resetLink);
            byId("resetLinkDisplay").innerHTML = "Please pretend you actually got the following link sent to the email you entered<br>" +
                "<a href=\"" + response.resetLink + "\">Click to reset your password</a>";
        }
    }
}


function openRegistration() {
    byId("registrationModal").style.display = 'block';
}

function openReset() {
    byId("resetModal").style.display = 'block';
}

function closeRegistration() {
    byId("registrationModal").style.display = 'none';
    byId("registrationForm").reset();
    byId("registrationFailure").innerHTML = "";
}

function closeReset() {
    byId("resetModal").style.display = 'none';
    byId("resetForm").reset();
    byId("resetFailure").innerHTML = "";
    byId("resetLinkDisplay").innerHTML = "";
}


//                       The message module                                  //
//---------------------------------------------------------------------------//

/**
 * MessageModule collects functions for construction, addition, removal and display of messages
 * @type {{set, add, show, remove}}
 */
var messagesModule = (function() {
    var messages = [];
    var userFilter = '';

    /**
     * Sets the internal message array.
     * @param m Array of messages from PHP.
     */
    function set(m) { messages = m; }

    /**
     * Function for addition of a new message object to the messageModule's internal array.
     * @param m The message object to add.
     */
    function add(m) { messages.push(m); }


    /**
     * Handles the display of messages.
     * @param loggedInUser A looged in user or empty string for no logged in user.
     */
    function show(loggedInUser) {
        var sortMethod = document.getElementById('sortBySelect').value;

        //sorting
        if (sortMethod === 'Date') {
            sortByDate();
        }
        else if (sortMethod === 'Popularity') {
            sortByVotes();
        }

        // Filter
        userFilter = document.getElementById('showUserTextBox').value;
        var filteredMessages = [];
        for (var i = 0; i < messages.length; i++) {
            if (userFilter === '') {
                filteredMessages.push(messages[i]);
            } else if (messages[i]['author'].includes(userFilter)) {
                filteredMessages.push(messages[i])
            }
        }

        //search by keyword
        var searchWord = byId('searchWordTextBox').value;
        if (searchWord !== '')
            filteredMessages = filterBySearchWord(filteredMessages, searchWord);

        //display messages on screen
        var root = byId('readMessageArea');
        root.innerHTML = '';
        for (var i = 0; i < filteredMessages.length; i++) {
            var message = messageBuilder(filteredMessages[i], loggedInUser);
            root.appendChild(message);
        }
    }

    /**
     * Function for deletion of a message.
     * @param id The message to be deleted
     */
    function remove(id) {
        messages = messages.filter(
            function(message) {
                return message['message_id'] !== id;
            }
        );

        var element = byId(id);
        element.parentNode.removeChild(element);
    }


    /**
     * Sorts the message list by popularity.
     */
    function sortByVotes() {
        messages.sort(
            function(a, b) {
                if (a['voteCount'] > b['voteCount'])
                    return -1;
                else if (a['voteCount'] < b['voteCount'])
                    return 1;
                return 0;
            }
        );
    }

    /**
     * Sorts the message list by date.
     */
    function sortByDate() {
        messages.sort(
            function(a, b) {
                if (a['datetime'] < b['datetime'])
                    return -1;
                else if (a['datetime'] > b['datetime'])
                    return 1;
                return 0;
            }
        );
    }

    /**
     * Function for filtering the message list based on a keyword entered by user.
     * @param messages An array of current message objects.
     * @param searchWord The word to search for in messages.
     * @returns {Array} An array of messages containing the search word.
     */
    function filterBySearchWord(messages, searchWord) {
        var hits = [];
        for (var i = 0; i < messages.length; i++) {
            var messageString = messages[i]['message'];
            if (messageString.indexOf(searchWord) !== -1)
                hits.push(messages[i]);
        }
        return hits;
    }

    /**
     * Hnadles selection of which type of upvote button to display.
     * @param loggedInUser Username of a logged in user or empty string in not logged in.
     * @param author The author of current message.
     * @param vote Upvote or downvote performed by user.
     * @param voteButton The button element.
     * @returns {string} The file name of the button image to display.
     */
    function getUpVoteButton(loggedInUser, author, vote, voteButton) {
        if ((loggedInUser === '' && vote === false) || (loggedInUser !== '' && ((loggedInUser === author) || vote === -1))) {
            voteButton.disabled = true;
            if (loggedInUser === '')
                voteButton.title = 'Login to vote on a message';
            else if (loggedInUser === author)
                voteButton.title = 'Voting on own messages is not allowed';
            else if (vote === -1)
                voteButton.title = 'You have already voted on this message';
            return 'img/upVote_disabled.png';
        }
        else if (vote === 1) {
            voteButton.disabled = true;
            voteButton.title = 'You have already voted on this message';
            return 'img/upVoted.png';
        }
        voteButton.addEventListener('click', function() { doVoteMessage(loggedInUser, voteButton) }, false);
        return 'img/upVote.png';
    }

    /**
     * Hnadles selection of which type of downvote button to display.
     * @param loggedInUser Username of a logged in user or empty string in not logged in.
     * @param author The author of current message.
     * @param vote Upvote or downvote performed by user.
     * @param voteButton The button element.
     * @returns {string} The file name of the button image to display.
     */
    function getDownVoteButton(loggedInUser, author, vote, voteButton) {
        if ((loggedInUser === '' && vote === false) || (loggedInUser !== '' && ((loggedInUser === author) || vote === 1))) {
            voteButton.disabled = true;
            if (loggedInUser === '')
                voteButton.title = 'Login to vote on a message';
            else if (loggedInUser === author)
                voteButton.title = 'Voting on own messages is not allowed';
            else if (vote === 1)
                voteButton.title = 'You have already voted on this message';
            return 'img/downVote_disabled.png';
        }
        else if (vote === -1) {
            voteButton.disabled = true;
            voteButton.title = 'You have already voted on this message';
            return 'img/downVoted.png';
        }
        voteButton.addEventListener('click', function() { doVoteMessage(loggedInUser, voteButton) }, false);
        return 'img/downVote.png';
    }

    /**
     * Function for creation of a message.
     * @param message Object holding information about the message to be created.
     * @param loggedInUser Username of a logged in user or empty string in not logged in.
     * @returns {*} Returns the created message in div container.
     */
    function messageBuilder(message, loggedInUser) {
        var messageContainer = elem('div');
        messageContainer.id = message['message_id'];
        messageContainer.classList.add('messageContainer');
        messageContainer.setAttribute('data-author', message['author']);

        //voteContainer
        var voteContainer = elem('div');
        voteContainer.classList.add('voteDiv');
        var upVote = elem('input');
        upVote.type = 'image';
        upVote.alt = 'upvote';
        upVote.classList.add('upVoteButton');

        var voteCount = elem('p');
        voteCount.classList.add('voteCount');
        voteCount.innerText = message['voteCount'];
        var downVote = elem('input');
        downVote.type = 'image';
        downVote.alt = 'downvote';
        downVote.classList.add('downVoteButton');

        voteContainer.appendChild(upVote);
        voteContainer.appendChild(voteCount);
        voteContainer.appendChild(downVote);

        //messageTextContainer
        var messageTextContainer = elem('div');
        messageTextContainer.classList.add('messageText');
        var messageData = elem('div');
        messageData.classList.add('messageData');
        var messageContent = elem('div');
        messageContent.classList.add('messageContent');

        var author = elem('span');
        author.classList.add('author');
        author.innerText = message['author'];
        var dateTime = elem('span');
        dateTime.classList.add('dateTime');
        dateTime.innerText = message['datetime'];
        var messageText = elem('p');
        messageText.classList.add('theMessage');
        messageText.innerText = message['message'];

        messageData.appendChild(author);
        messageData.appendChild(dateTime);
        messageContent.appendChild(messageText);
        messageTextContainer.appendChild(messageData);
        messageTextContainer.appendChild(messageContent);

        //deleteContainer
        var deleteContainer = elem('div');
        deleteContainer.classList.add('deleteContainer');
        var deleteButton = elem('button');
        deleteButton.type = 'button';
        deleteButton.classList.add('deleteButton');
        deleteButton.innerHTML = 'Delete';
        deleteButton.style.display = showDeleteButton(deleteButton, loggedInUser, message['author']);
        deleteContainer.appendChild(deleteButton);

        messageContainer.appendChild(voteContainer);
        messageContainer.appendChild(messageTextContainer);
        messageContainer.appendChild(deleteContainer);

        upVote.src = getUpVoteButton(loggedInUser, message['author'], message['userVote'], upVote);
        downVote.src = getDownVoteButton(loggedInUser, message['author'], message['userVote'], downVote);

        return messageContainer;
    }

    /**
     * Determination of whether a delete button should be displayed in the message being created.
     * @param deleteButton The delete button element.
     * @param loggedInUser Username of a logged in user or empty string in not logged in.
     * @param author The author of current message.
     * @returns {string} Returns a string for the display style to be used.
     */
    function showDeleteButton(deleteButton, loggedInUser, author) {
        if (loggedInUser !== '') {
            if (loggedInUser === author) {
                deleteButton.addEventListener('click', doDeleteMessage, false);
                return 'initial';
            }
            else {
                deleteButton.removeEventListener('click', doDeleteMessage, false);
                return 'none';
            }
        }
    }


    // public methods of the MessageModule.
    return {
        set: set,
        add: add,
        show: show,
        remove: remove
    }
}());