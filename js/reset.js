/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: reset.js
 * Desc: Javascript code for the password reset page
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

var xhr_reset;

//Utilities
function byId(id) {
    return document.getElementById(id);
}

function main() {
    byId("resetButton").addEventListener('click', doPasswordReset, false);

    // Initialize xhr object
    try {
        if (window.XMLHttpRequest) {
            xhr_reset = new XMLHttpRequest();
        }
        else if (window.ActiveXObject) {
            xhr_reset = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            throw new Error('Cannot create XMLHttpRequest object');
        }
    } catch (e) {
        alert('"XMLHttpRequest failed!' + e.message);
    }
}
window.addEventListener("load", main, false);

function doPasswordReset() {
    var post = true,
        password = byId("psw").value,
        passwordConfirm = byId("pswdConfirm").value,
        email = byId("email").value,
        selector = byId("selector").value,
        token = byId("token").value;

    if (password <= 0) {
        byId("resetFailure").innerHTML += "You must choose a new password<br>";
        post = false;
    }
    if (password !== passwordConfirm) {
        byId("resetFailure").innerHTML += "Passwords do not match<br>";
        post = false;
    }

    if (post) {
        // Create formdata
        var formData = new FormData();

        formData.append('password', password);
        formData.append('email', email);
        formData.append('token', token);
        formData.append('selector', selector);

        // create xhr request
        xhr_reset.addEventListener('readystatechange', processPasswordReset, false);
        xhr_reset.open('POST', 'execute_reset.php', true);
        xhr_reset.send(formData);
    }
    else {
        byId("psw").value = "";
        byId("pswdConfirm").value = "";
    }
}

function processPasswordReset() {
    if (xhr_reset.readyState === XMLHttpRequest.DONE && xhr_reset.status === 200) {
        xhr_reset.removeEventListener('readystatechange', processPasswordReset, false);

        var response = JSON.parse(this.responseText);

        if(!response.result) {
            for (var i = 0; i < response.errors.length; i++) {
                byId("resetFailure").innerHTML += response.errors[i] + "<br>";
            }
            byId("psw").value = "";
            byId("pswdConfirm").value = "";
        }

        if(response.result) {
            byId("resetForm").style.display = 'none';
            byId("confirmationMessage").innerHTML = "Your password has been successfully updated.<br>" +
                "<a href=\"index.php\">Click here</a>" + " to return to the start page.";
        }
    }
}