<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: request_pw_reset.php
 * Desc: Password reset request script for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

$email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
$errors = array();
$resetOk = true;
$responseText = array();

//Check ensuring the email is correctly formatted.
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    array_push($errors, "Invalid email format.");
    $resetOk = false;
}

if ($resetOk) {
    $db = Database::getInstance();
    $db->connect();
    $result = $db->emailInUse($email);

    if (!$result['success']) {
        array_push($errors, "Failed to query the database.");
        $resetOk = false;
    }
    elseif ($result['emailFound']) {
        $result = $db->schedulePasswordReset($email);

        if($result["success"] == false) {
            array_push($errors, "Failed to query the database.");
            $resetOk = false;
        }
        else {
            $responseText['result'] = true;
            $responseText['resetLink'] = "pw_reset.php?email=" . $_POST["email"] . "&token="
                . $result['token'] . "&select=" . $result['selector'];
        }
    }
    else {
        array_push($errors, "There is no account associated with this email address.");
        $resetOk = false;
    }

    $db->disconnect();

}

if(!$resetOk) {
    $responseText['errors'] = $errors;
    $responseText['result'] = false;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);

?>