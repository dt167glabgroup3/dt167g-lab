<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: pw_reset.php
 * Desc: Password reset page for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

if(isset($_SESSION["pswReset"])) {
    unset($_SESSION["pswReset"]);
}

$title = "Twitter2";
$email = $_GET["email"];
$selector = $_GET["select"];
$token = $_GET["token"];
$status = "";

if ($email == "" || $token == "" || $selector == "") {

}
else {
    //Does a somewhat redundant check against the db if the supplied link is valid for resetting passwords since this check
    // is done again in the execute_reset.php script. Mainly for debugging purposes.
    $db = Database::getInstance();
    $db->connect();
    $result =  $db->validatePasswordReset($email, $token, $selector);

    if (!$result["success"]) {
        $status = "query_failed";
    }
    elseif ($result["expired"]) {
        $status ="expired";
    }
    elseif ($result["validation"]) {
        $status = "ok";
    }
    else {
        $status = "validation_failed";
    }

    $db->disconnect();
}

?>


<!DOCTYPE html>
<html lang="sv-SE">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/reset.js"></script>
</head>
<body>
<header>
    <img src="img/earth72.png" alt="earth" class="logo"/>
    <div id="pageTitle"><h1><?php echo $title ?></h1></div>

</header>
<main>
    <div class="mainContent">
        <?php
            if($status == "query_failed") {
                echo "Failed to query the database, please try again later.";
            }
            elseif($status == "validation_failed") {
                echo "Validation of the request failed. Incorrect credentials were supplied.";
            }
            elseif ($status == "expired") {
                echo "The link has expired";
            }
            elseif ($status == "ok") {
                echo "<form id=\"resetForm\">";
                echo "<label><b>Password</b></label>";
                echo "<input type=\"password\" placeholder=\"Enter Password\" name=\"psw\" id=\"psw\" required><br>";
                echo "<label><b>Confirm password</b></label>";
                echo "<input type=\"password\" id=\"pswdConfirm\" placeholder=\"Re-enter Password\" required><br>";
                echo "<input type=\"hidden\" id=\"token\" value=\"" . htmlspecialchars($token) ."\">";
                echo "<input type=\"hidden\" id=\"selector\" value=\"" . htmlspecialchars($selector) ."\">";
                echo "<input type=\"hidden\" id=\"email\" value=\"" . htmlspecialchars($email) ."\">";
                echo "<button type=\"button\" id=\"resetButton\">Change password</button>";
                echo "<span id=\"resetFailure\" class=\"red\"></span>";
                echo "</form>";
            }
        ?>
        <span id="confirmationMessage">

        </span>
    </div>
</main>
<footer>
    <div class="footer">
        <p><span class="blue">&copy; Twitter2</span></p>
        <p>by <a href="mailto:mihe1602@student.miun.se">twitter2team</a></p>
        <p>All rights reserved.</p>
    </div>
</footer>
</body>
</html>
