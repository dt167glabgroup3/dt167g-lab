<?php
/**
 * Created by PhpStorm.
 * User: Mikael
 * Date: 2018-05-14
 * Time: 23:14
 */


// This array holds the links to be displayed when a user has logged out
$link_array = [
    "Gästbok" => "guestbook.php",
];


session_start();

// Unset all of the session variables.
$_SESSION = array();

/**
 * On logout delete the session cookie.
 *  Note: This will destroy the session, and not just the session data!
 */
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// Finally, destroy the session.
session_destroy();

$responseText["displayMessage"] = '';
$responseText["links"] = $link_array;
header('Content-Type: application/json');
echo json_encode($responseText);

?>