<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: register.php
 * Desc: Registration script for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

$config = Config::getInstance();
$username = $_POST["username"];
$email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
$password = $_POST["password"];
$errors = array();
$registrationOk = true;


//Check ensuring the username contains no forbidden characters.
if (!preg_match($config->getLegalCharacters(), $username)) {
    array_push($errors, "Username may only contain alphanumeric characters and underscores(_)");
    $registrationOk = false;
}

//Check ensuring the username is within allowed size limitations.
if (strlen($username) > $config->getUsernameLimit()) {
    array_push($errors, "Username may not be longer than 12 characters");
    $registrationOk = false;
}

//Check ensuring the email is correctly formatted.
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    array_push($errors, "Invalid email format");
    $registrationOk = false;
}

if ($registrationOk) {
    $db = Database::getInstance();
    $db->connect();

    //Check that neither the supplied username nor email are already in the db
    $result = $db->usernameExists($username);

    if ($result['success'] == false) {
        array_push($errors, "Failed to query the database.");
        $registrationOk = false;
    }
    elseif ($result['userFound'] == true) {
        array_push($errors, "A user with that name already exists.");
        $registrationOk = false;
    }

    $result = $db->emailInUse($email);

    if ($result['success'] == false) {
        array_push($errors, "Failed to query the database.");
        $registrationOk = false;
    }
    elseif ($result['emailFound'] == true) {
        array_push($errors, "This email address has already been used to create an account.");
        $registrationOk = false;
    }

    if ($registrationOk) {
        $result = $db->addUser($username, $email, $password);

        if(!$result) {
            array_push($errors, "Database error, something went wrong adding new user to the database");
            $registrationOk = false;
        }
    }

    $db->disconnect();

}

if ($registrationOk) {
    $responseText['result'] = true;
}
else {
    $responseText['result'] = false;
    $responseText['errors'] = $errors;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);

?>