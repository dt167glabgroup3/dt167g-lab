<?php
/*******************************************************************************
 * Laboration, Kurs: DT167G
 * File: pw_reset.php
 * Desc: Password reset script for the lab assignment
 *
 * Robin Löfås
 * rolo1102
 * rolo1102@student.miun.se
 ******************************************************************************/

include_once 'util.php';

session_start();

$email = html_entity_decode($_POST["email"]);
$password = $_POST["password"];
$selector = html_entity_decode($_POST["selector"]);
$token = html_entity_decode($_POST["token"]);
$errors = array();
$resetOk = true;

$db = Database::getInstance();
$db->connect();
$validation = $db->validatePasswordReset($email, $token, $selector);

if (!$validation["success"]) {
    array_push($errors, "Failed to query the database");
    $resetOk = false;
}
elseif ($validation["expired"]) {
    array_push($errors, "Reset request expired.");
    $resetOk = false;
}
elseif (!$validation["validation"]) {
    array_push($errors, "Validation of the request failed.");
    $resetOk = false;

}

if ($resetOk) {
    $result = $db->changePassword($email, $password);

    if(!$result) {
        $resetOk = false;
    }
    else {
        $responseText['result'] = true;
    }
}

$db->disconnect();

if (!$resetOk) {
    $responseText["errors"] = $errors;
    $responseText['result'] = false;
}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($responseText);

?>